Small app that shows a list of users and through long polling updates it when a user is created or edited. The users are stored in the SQLite3 database in the root folder named `test.db`.

## Task
Implement web server in nodejs using your favorite HTTP framework and sqlite DB. The web server should manage list of users persistently. The server should provide interface to view list of users. The server should allow to add new user. When new user added, all the web clients should display immediately the added user in the list.

## Setup

To install the application please run the following command:

```
npm install
```

Afterwards you can start the application with:

```
npm start
```

## Requirements

Please make sure you have Node.js 6 or newer installed on your device before running the application.
