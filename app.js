const express = require('express');
const path = require('path');
const exphbs  = require('express-handlebars');
const logger = require('morgan');
const bodyParser = require('body-parser');
const Database = require('better-sqlite3');

global.db = new Database('test.db');

// include web routes
const index = require('./routes/web/index');
const usersWeb = require('./routes/web/users');

// include API routes
const usersAPI = require('./routes/api/users');

const app = express();
global.longpoll = require('express-longpoll')(app);
longpoll.create('/user-poll');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// register routes
app.use('/', index);
app.use('/users', usersWeb);

// register API routes
app.use('/api/users', usersAPI);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

module.exports = app;
