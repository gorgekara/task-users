const express = require('express');
const router = express.Router();
const userController = require('../../controllers/web/user');

router.get('/', userController.index);
router.get('/create', userController.create);
router.get('/:id', userController.edit);

router.post('/', userController.save);
router.post('/:id/update', userController.update);

module.exports = router;
