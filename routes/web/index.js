const express = require('express');
const router = express.Router();
const indexController = require('../../controllers/web/index');

router.get('/', indexController.index);

module.exports = router;
