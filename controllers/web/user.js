const dblUser = require('../../dbl/user');
const controller = {};

/**
 * GET /users
 * Users home page, returns only the user index view
 */
controller.index = (req, res, next) => {
  let context = {
    menuUsers: 'active'
  };

  res.render('users/index', context);
};


/**
 * GET /users/create
 * Users create page, returns only the user create view
 */
controller.create = (req, res, next) => {
  res.render('users/user');
};

/**
 * GET /users/:id
 * Users edit page uses the same view as create
 * with the difference that the user is returned as well.
 */
controller.edit = (req, res, next) => {
  let user = dblUser.get(req.params.id);

  if (!user) {
    return res.render('error');
  }

  res.render('users/user', user);
};


/**
 * POST /users/:id/update
 * Updates the user whose id is specified in the url
 */
controller.update = (req, res, next) => {
  let { username, email, first_name, last_name, age } = req.body;
  let context = { username, email, first_name, last_name, age };

  let id = req.params.id;

  if (!username || !email || !first_name || !last_name || !age) {
    context.error = 'Please enter all required fields';
    return res.render('users/user', context);
  }

  try {
    let result = dblUser.update(id, context);

    context.id = id;

    /**
     * Publish 'user-poll' so all connected browsers to the user/index page
     * receive the updated data for the user being updated.
     */
    longpoll.publish('/user-poll', context);

    return res.redirect(`/users/${id}`);
  } catch (e) {
    context.error = 'Could not update user at this time';
    return res.render('users/user', context);
  }
};


/**
 * POST /users
 * Creates a new user with the data provided
 */
controller.save = (req, res, next) => {
  let { username, email, first_name, last_name, age } = req.body;
  let context = { username, email, first_name, last_name, age };

  if (!username || !email || !first_name || !last_name || !age) {
    context.error = 'Please enter all required fields';
    return res.render('users/user', context);
  }

  try {
    let result = dblUser.create(context);
    context.id = result.lastInsertROWID;

    /**
     * Publish 'user-poll' so all connected browsers to the user/index page
     * receive the new user data and the user is added to the table.
     */
    longpoll.publish('/user-poll', context);

    return res.redirect(`/users/${context.id}`);
  } catch (e) {
    context.error = 'Could not create user at this time';
    return res.render('users/user', context);
  }
};

module.exports = controller;
