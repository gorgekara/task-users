const controller = {};

controller.index = (req, res, next) => {
  res.render('index', {
    menuHome: 'active',
  });
};

module.exports = controller;
