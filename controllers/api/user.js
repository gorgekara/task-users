const dblUser = require('../../dbl/user');
const controller = {};

/**
 * Small API endpoint that fetches all users
 * and returns them in JSON format
 */
controller.index = (req, res, next) => {
  let users = dblUser.index();
  res.json(users);
};

module.exports = controller;
