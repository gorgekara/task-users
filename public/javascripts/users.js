(($) => {
  'use strict';

  let $loading = $('.e-loading');
  let $noResults = $('.e-no-results');

  /**
   * Since it's a small task I refrained from using other frameworks
   * like React or Angular to handle the template below. If this project
   * grows I'd recommend switching for better maintainability.
  */
  const rowTemplate = ({ id, username, first_name, last_name, email, age }) => {
    return `
      <tr id="user-${id}">
        <td>${id}</td>
        <td>${username}</td>
        <td>${first_name}</td>
        <td>${last_name}</td>
        <td>${email}</td>
        <td>${age}</td>
        <td>
          <a href="/users/${id}">Edit</a>
        </td>
      </tr>
    `;
  };


  /**
   * Appends the newly created user to the end of the table
   */
  const appendToRow = (user) => {
    let $userTable = $('.e-user-table');
    let $userTableBody = $userTable.find('tbody');
    let template = rowTemplate(user);

    $userTableBody.append(template);
  };


  /**
   * Finds and updates only the specific row (which another user is editing)
   */
  const updateRow = (user) => {
    let $userRow = $(`#user-${user.id}`);
    let template = rowTemplate(user);

    $userRow.replaceWith(template);
  };


  /**
   * Get a list of users from the backend API endpoint and
   * using the appendToRow function add them to the table.
  */
  const getUsers = () => {
    $loading.show();
    $noResults.hide();

    $.ajax({ url: '/api/users' }).then((response) => {
      $loading.hide();

      if (!response.length) {
        $noResults.show();
        return;
      }

      (response || []).forEach((user) => appendToRow(user));
    });
  };


  /**
   * Polling function that makes ajax request to the backend API endpoint
   * that handles the long polling of resources (in this case newly created users).
  */
  const pollUsers = () => {
    $.ajax({
       url: '/user-poll',
       success: (data) => {

          if ($(`#user-${data.id}`).length) {
            updateRow(data);
          } else {
            appendToRow(data);
          }

          pollUsers();
       },
       error: () => {
          pollUsers();
       },
       timeout: 30000
    });
  };

  pollUsers();
  getUsers();

})(jQuery);
