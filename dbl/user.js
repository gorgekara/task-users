const dbl = {};

dbl.index = () => {
  return db.prepare('SELECT * FROM users').all();
};

dbl.get = (userId) => {
  return db.prepare('SELECT * FROM users WHERE id = ?').get(userId);
};

dbl.create = ({ username, email, first_name, last_name, age }) => {
  let stmt = db.prepare('INSERT INTO users (username, email, first_name, last_name, age) VALUES (?, ?, ?, ?, ?)');

  return stmt.run(username, email, first_name, last_name, age);
};

dbl.update = (id, { username, email, first_name, last_name, age }) => {
  let stmt = db.prepare('UPDATE users SET username = ?, email = ?, first_name = ?, last_name = ?, age = ? WHERE id = ?');

  return stmt.run(username, email, first_name, last_name, age, id);
};

module.exports = dbl;